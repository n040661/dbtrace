package john.walker.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import john.walker.Monitor;

/**
 * 打印当前未关闭的连接栈
 *
 * @author 30san
 *
 */
public class ConnectionLeakServlet extends HttpServlet {

	/**
	 *
	 */
	private static final long serialVersionUID = 7613061533432746997L;


	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		PrintWriter out = null;
		res.setCharacterEncoding("utf-8");
		try {
			out = res.getWriter();
			Monitor.getMonitor().printStackTrace(out);
		} finally {
			if(out != null) {
				out.flush();
				out.close();
			}
		}
	}

}
